# Entscheidungsfinder

Ein kleines Board mit Mikrocontroller, das zufällig Ja, Nein oder Vielleicht 
Entscheidungen per LEDs ausgibt.

Die Idee ist, einen Lötkurs für Lötanfänger anbieten zu können, in welchem man 
sich Grundkenntnisse über das THT- (bedrahtete) Löten aneignen kann. Anders als 
die meisten anderen Lötkurse setzt dieser nicht auf eine NE555 Blinkschaltung, 
sondern ein einigermaßen nützliches Gadget. Die erste Version setzte noch auf 
Streifenrasterplatinen. In Zuge dieses Projekts wurde allerdings ein eigenes 
Platinenlayout entworfen und die Software überarbeitet.

#### Roadmap
- [x] Schaltplan entwerfen
- [x] Board layouten
- [x] Software schreiben
- [x] Prototypen herstellen (ätzen, bohren, bestücken, löten)
- [x] Langzeittest durchführen
- [x] in Fertigung geben
- [ ] Anleitung schreiben
- [ ] Lötkurs anbieten

#### Features
  - einfach aufzubauen
  - wenige, günstige Komponenten
  - kleiner Formfaktor
  - lange Laufzeit
  - halbwegs nützlich
