EESchema Schematic File Version 2
LIBS:entscheidungsfinder-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:entscheidungsfinder-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "9 jun 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ATTINY85-P IC1
U 1 1 5576BC7F
P 5500 3300
F 0 "IC1" H 4350 3700 40  0000 C CNN
F 1 "ATTINY85-P" H 6500 2900 40  0000 C CNN
F 2 "DIP8" H 6500 3300 35  0000 C CIN
F 3 "~" H 5500 3300 60  0000 C CNN
	1    5500 3300
	1    0    0    -1  
$EndComp
$Comp
L LED-RESCUE-entscheidungsfinder D1
U 1 1 5576C42A
P 2950 3050
F 0 "D1" H 2950 3150 50  0000 C CNN
F 1 "RED" H 2950 2950 50  0000 C CNN
F 2 "~" H 2950 3050 60  0000 C CNN
F 3 "~" H 2950 3050 60  0000 C CNN
	1    2950 3050
	-1   0    0    -1  
$EndComp
$Comp
L LED-RESCUE-entscheidungsfinder D2
U 1 1 5576C458
P 3250 3150
F 0 "D2" H 3250 3250 50  0000 C CNN
F 1 "YELLOW" H 3250 3050 50  0000 C CNN
F 2 "~" H 3250 3150 60  0000 C CNN
F 3 "~" H 3250 3150 60  0000 C CNN
	1    3250 3150
	-1   0    0    -1  
$EndComp
$Comp
L LED-RESCUE-entscheidungsfinder D3
U 1 1 5576C467
P 3450 3350
F 0 "D3" H 3450 3450 50  0000 C CNN
F 1 "GREEN" H 3450 3250 50  0000 C CNN
F 2 "~" H 3450 3350 60  0000 C CNN
F 3 "~" H 3450 3350 60  0000 C CNN
	1    3450 3350
	-1   0    0    -1  
$EndComp
$Comp
L R-RESCUE-entscheidungsfinder R1
U 1 1 5576C59D
P 3600 3050
F 0 "R1" V 3680 3050 40  0000 C CNN
F 1 "100Ω" V 3607 3051 40  0000 C CNN
F 2 "~" V 3530 3050 30  0000 C CNN
F 3 "~" H 3600 3050 30  0000 C CNN
	1    3600 3050
	0    -1   -1   0   
$EndComp
$Comp
L R-RESCUE-entscheidungsfinder R2
U 1 1 5576C5AA
P 3900 3150
F 0 "R2" V 3980 3150 40  0000 C CNN
F 1 "100Ω" V 3907 3151 40  0000 C CNN
F 2 "~" V 3830 3150 30  0000 C CNN
F 3 "~" H 3900 3150 30  0000 C CNN
	1    3900 3150
	0    -1   -1   0   
$EndComp
$Comp
L R-RESCUE-entscheidungsfinder R3
U 1 1 5576C5B0
P 3900 3350
F 0 "R3" V 3980 3350 40  0000 C CNN
F 1 "100Ω" V 3907 3351 40  0000 C CNN
F 2 "~" V 3830 3350 30  0000 C CNN
F 3 "~" H 3900 3350 30  0000 C CNN
	1    3900 3350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3450 3150 3650 3150
Wire Wire Line
	3850 3050 4150 3050
Wire Wire Line
	3150 3050 3350 3050
Wire Wire Line
	3050 3150 2750 3150
Wire Wire Line
	2750 3350 3250 3350
Connection ~ 2750 3150
$Comp
L SW_PUSH SW1
U 1 1 5576C6F4
P 3750 3650
F 0 "SW1" H 3900 3760 50  0000 C CNN
F 1 "SW_PUSH" H 3750 3570 50  0000 C CNN
F 2 "~" H 3750 3650 60  0000 C CNN
F 3 "~" H 3750 3650 60  0000 C CNN
	1    3750 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 3250 4100 3250
Wire Wire Line
	4100 3250 4100 3650
Wire Wire Line
	4100 3650 4100 3800
Wire Wire Line
	4100 3650 4050 3650
$Comp
L R-RESCUE-entscheidungsfinder R4
U 1 1 5576C71C
P 4350 3800
F 0 "R4" V 4430 3800 40  0000 C CNN
F 1 "10kΩ" V 4357 3801 40  0000 C CNN
F 2 "~" V 4280 3800 30  0000 C CNN
F 3 "~" H 4350 3800 30  0000 C CNN
	1    4350 3800
	0    -1   -1   0   
$EndComp
Connection ~ 4100 3650
Wire Wire Line
	2750 3650 3450 3650
Connection ~ 2750 3350
Wire Wire Line
	4600 3800 4750 3800
Connection ~ 2750 3650
NoConn ~ 4150 3450
NoConn ~ 4150 3550
$Comp
L BATTERY BT1
U 1 1 5576CA0D
P 5400 2050
F 0 "BT1" H 5400 2250 50  0000 C CNN
F 1 "BATTERY" H 5400 1860 50  0000 C CNN
F 2 "~" H 5400 2050 60  0000 C CNN
F 3 "~" H 5400 2050 60  0000 C CNN
	1    5400 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 3050 2750 3150
Wire Wire Line
	2750 3150 2750 3350
Wire Wire Line
	2750 3350 2750 3650
Wire Wire Line
	2750 3650 2750 3850
$Comp
L VCC #PWR01
U 1 1 5576CBCC
P 7050 3000
F 0 "#PWR01" H 7050 3100 30  0001 C CNN
F 1 "VCC" H 7050 3100 30  0000 C CNN
F 2 "" H 7050 3000 60  0000 C CNN
F 3 "" H 7050 3000 60  0000 C CNN
	1    7050 3000
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR02
U 1 1 5576CBDB
P 4950 1900
F 0 "#PWR02" H 4950 2000 30  0001 C CNN
F 1 "VCC" H 4950 2000 30  0000 C CNN
F 2 "" H 4950 1900 60  0000 C CNN
F 3 "" H 4950 1900 60  0000 C CNN
	1    4950 1900
	1    0    0    -1  
$EndComp
$Comp
L GND-RESCUE-entscheidungsfinder #PWR03
U 1 1 5576CBEA
P 5850 2200
F 0 "#PWR03" H 5850 2200 30  0001 C CNN
F 1 "GND" H 5850 2130 30  0001 C CNN
F 2 "" H 5850 2200 60  0000 C CNN
F 3 "" H 5850 2200 60  0000 C CNN
	1    5850 2200
	1    0    0    -1  
$EndComp
$Comp
L GND-RESCUE-entscheidungsfinder #PWR04
U 1 1 5576CC17
P 2750 3850
F 0 "#PWR04" H 2750 3850 30  0001 C CNN
F 1 "GND" H 2750 3780 30  0001 C CNN
F 2 "" H 2750 3850 60  0000 C CNN
F 3 "" H 2750 3850 60  0000 C CNN
	1    2750 3850
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR05
U 1 1 5576CC3A
P 4750 3800
F 0 "#PWR05" H 4750 3900 30  0001 C CNN
F 1 "VCC" H 4750 3900 30  0000 C CNN
F 2 "" H 4750 3800 60  0000 C CNN
F 3 "" H 4750 3800 60  0000 C CNN
	1    4750 3800
	1    0    0    -1  
$EndComp
$Comp
L GND-RESCUE-entscheidungsfinder #PWR06
U 1 1 5576CC67
P 7000 3600
F 0 "#PWR06" H 7000 3600 30  0001 C CNN
F 1 "GND" H 7000 3530 30  0001 C CNN
F 2 "" H 7000 3600 60  0000 C CNN
F 3 "" H 7000 3600 60  0000 C CNN
	1    7000 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6850 3550 7000 3550
Wire Wire Line
	7000 3550 7000 3600
Wire Wire Line
	6850 3050 7050 3050
Wire Wire Line
	7050 3050 7050 3000
Wire Wire Line
	5700 2050 5850 2050
Wire Wire Line
	5850 2050 5900 2050
Wire Wire Line
	5850 2050 5850 2200
Wire Wire Line
	4950 1900 4950 2050
Wire Wire Line
	4950 2050 5100 2050
$Comp
L CONN_1 GND1
U 1 1 55771AF4
P 6050 2050
F 0 "GND1" H 6130 2050 40  0000 L CNN
F 1 "CONN_1" H 6050 2105 30  0001 C CNN
F 2 "~" H 6050 2050 60  0000 C CNN
F 3 "~" H 6050 2050 60  0000 C CNN
	1    6050 2050
	1    0    0    -1  
$EndComp
Connection ~ 5850 2050
$EndSCHEMATC
