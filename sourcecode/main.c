/*
 * main.c
 *
 *  Created on: 13.05.2015
 *      Author: Matthias Köferlein (matthias.koeferlein@gmail.com)
 *        Note: This code requires a (relatively) recent version of avr-libc.
 *              If you get errors concerning 'EMPTY_INTERRUPT()', update your
 * 				avr-libc.
 * 				Due to delay.h being used, you must set the optimization
 * 				level to at least 'slight optimizations' (-O1 compiler
 * 				flag), else you will get errors and the code won't work.
 * 	   License:	This code is released into the public domain. Of course
 * 				it would be nice if you were to credit me or share the
 * 				project you used this for to let me know I'm doing use-
 * 				full stuff.
 */

#ifndef F_CPU
#define F_CPU 1000000
#endif

#include <avr/interrupt.h>
// I don't actually know whether this is necessary, but I'll keep it.
// If you plan on using a different chip, edit this accordingly.
#include <avr/iotn85.h>
#include <avr/sleep.h>
#include <stdlib.h>
#include <util/delay.h>

// Macro for creating an IRS that does nothing and immediately
// returns to normal program execution
// In this case it's the IRS for the INT0 interrupt vector
EMPTY_INTERRUPT(INT0_vect);

#if FANCY_SEEDING
// Taken from ATtiny85 manual page 19
unsigned char EEPROM_read(unsigned char ucAddress) {
	// Wait for completion of previous write
	while (EECR & (1 << EEPE)) {

	}
	// Set up address register
	EEAR = ucAddress;
	// Start eeprom read by writing EERE
	EECR |= (1 << EERE);
	// Return data from data register
	return EEDR;
}
#else
#pragma message "You can enable FANCY_SEEDING by setting the flag."
#endif

// Program entry point
int main() {
	// Set portpins PB0, PB1 and PB3 as output for the LEDs
	DDRB |= (1 << PB0) | (1 << PB1) | (1 << PB3);
	// Set portpin PB2 as input for the button (PB2 is INT0 external interrupt)
	DDRB &= ~(1 << PB2);

	// Enable pullup for PB2 (button)
	PORTB |= (1 << PB2);

	// Set portpins PB0, PB1 and PB3 low
	PORTB &= ~((1 << PB0) | (1 << PB1) | (1 << PB3));

	// A little "Hello World" to test the setup
	// Blinks every LEDs once
	PORTB |= 1 << PB3;
	_delay_ms(500);
	PORTB &= ~(1 << PB3);
	PORTB |= 1 << PB1;
	_delay_ms(500);
	PORTB &= ~(1 << PB1);
	PORTB |= 1 << PB0;
	_delay_ms(500);
	PORTB &= ~(1 << PB0);

	// Seed random generator with the first byte of the EEPROM
	// You should write some random content to this byte during
	// programming the chip. On the Linux command line
	// "head -c 1 /dev/urandom" will get you one random byte.
	// Seeding this way yields a bit more randomness but is not
	// absolutely necessary. You can also write a fixed value
	// like 0xF00DBEEF here or something.
	#if FANCY_SEEDING
		srand(EEPROM_read(0x00));
	#else
		srand(0xF00DBEEF);
	#endif

	// Setup interrupt mode

	// MCU Control Register
	// Set external IRQ INT0 to be triggered when the portpin is pulled low
	// This interrupt can be triggered even if the device is in power-down mode
	// TODO: Changed this from '=' to '&='. '=' did seem to work, so if stuff breaks, change it back.
	MCUCR &= ~((1 << ISC00) | (1 << ISC01));

	// General Input Mask register
	// Enable external IRQ INT0
	GIMSK |= (1 << INT0);

	// Setup sleep mode
	// Set Sleep Enable SE to 1
	// Set Sleep Mode SM[1:0] to 10 for power down mode
	// Power down mode stops all internal clocks and uses <10µA
	MCUCR |= (1 << SE) | (1 << SM1);
	MCUCR &= ~(1 << SM0);

	// Main loop
	while (1) {
		// Enable interrupts
		sei();
		// Then go to sleep (until waking up when a interrupt fires)
		sleep_cpu();
		// After waking up disable interrupts
		cli();

		// Get a random number
		char temp = rand() % 3;
		// Display the result
		if (temp == 0) {
			// Display 'No' result
			PORTB |= 1 << PB0;
		} else if (temp == 1) {
			// Display 'Maybe' result
			PORTB |= 1 << PB1;
		} else {
			// Display 'Yes' result
			PORTB |= 1 << PB3;
		}

		// Keep displaying for 2 seconds
		_delay_ms(2000);

		// Switch all LEDs off
		PORTB &= ~((1 << PB0) | (1 << PB1) | (1 << PB3));
	}

	// Won't happen. Sad little return instruction... ;(
	return 0;
}
